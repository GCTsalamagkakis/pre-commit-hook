class StringUtils:

    @staticmethod
    def find_strings_in_list(strings, string_list):
        return [string for string in strings if any(list_item == string for list_item in string_list)]